<?php

namespace Klev\UserAuthentication;

class RemoveUserModel
{
    public static function execute()
    {
        $modelPath = base_path('app/Models/User.php');
        if(file_exists($modelPath)) {
            unlink($modelPath);
        }
    }
}
