<?php

namespace Klev\UserAuthentication;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:users,email|email',
            'password' => 'required|confirmed|min:8',
            'phone_number' => 'required|phone:AUTO',
            'profile' => 'required|file|mimes:jpg,png,jpeg|max:2000'
        ];
    }
}
