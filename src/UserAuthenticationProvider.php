<?php

namespace Klev\UserAuthentication;

use Illuminate\Support\ServiceProvider;

class UserAuthenticationProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Klev\UserAuthentication\UserController');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        RemoveUserModel::execute();
        RemoveUserMigration::execute();
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->publishes([
            __DIR__.'/UserController.php' => base_path('app/Http/Controllers/UserController.php'),
            __DIR__.'/UserService.php' => base_path('app/Services/UserService.php'),
            __DIR__.'/User.php' => base_path('app/Models/User.php'),
            __DIR__.'/UserObserver.php' => base_path('app/Observers/UserObserver.php'),
            __DIR__.'/ServiceTrait.php' => base_path('app/Traits/ServiceTrait.php'),
            __DIR__.'/LoginRequest.php' => base_path('app/Http/Requests/LoginRequest.php'),
            __DIR__.'/VerifyEmailRequest.php' => base_path('app/Http/Requests/VerifyEmailRequest.php'),
            __DIR__.'/CheckEmailRequest.php' => base_path('app/Http/Requests/CheckEmailRequest.php'),
            __DIR__.'/RegisterRequest.php' => base_path('app/Http/Requests/RegisterRequest.php'),
            __DIR__.'/migrations/2022_08_24_000000_create_users_table.php' => base_path('database/migrations/2022_08_24_000000_create_users_table.php'),
        ]);
    }
}
