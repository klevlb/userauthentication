<?php

namespace Klev\UserAuthentication;

use Exception;
use Illuminate\Support\Facades\Log;
use Klev\UserAuthentication\User;

class UserObserver
{
    /**
     * Handle the User "created" event.
     *
     * @param  \Klev\UserAuthentication\User  $user
     * @return void
     */
    public function created(User $user)
    {
        do {
            $otp = rand(100000, 999999);
        } while (User::where('otp', $otp)->first());

        $user->update(['otp' => $otp]);

        try {
            $user->sendEmailVerificationNotification();
        } catch (Exception $exception) {
            Log::error($exception);
        }
    }

    /**
     * Handle the User "updated" event.
     *
     * @param \Klev\UserAuthentication\User $user
     * @return void
     */
    public function updated(User $user)
    {
        //
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param  \Klev\UserAuthentication\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        //
    }

    /**
     * Handle the User "restored" event.
     *
     * @param  \Klev\UserAuthentication\User  $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the User "force deleted" event.
     *
     * @param  \Klev\UserAuthentication\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }
}
