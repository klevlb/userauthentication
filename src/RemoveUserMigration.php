<?php

namespace Klev\UserAuthentication;

class RemoveUserMigration
{
    public static function execute()
    {
        $migrationPath = base_path('database/migrations/2014_10_12_000000_create_users_table.php');
        if(file_exists($migrationPath)) {
            unlink($migrationPath);
        }
    }
}
