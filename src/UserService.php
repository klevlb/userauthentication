<?php

namespace Klev\UserAuthentication;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserService
{
    use ServiceTrait;

    public function login(Request $request)
    {
        $validatedData = $this->validate($request);
        $user = User::where('email', $validatedData['email'])->first();

        if (!$user || !Hash::check($validatedData['password'], $user->password)) {
            return $this->error('Wrong Email or Password');
        }

        $token = $user->createToken($validatedData['email'])->plainTextToken;

        return $this->success([
            'token_type' => 'bearer',
            'token' => $token,
            'user' => $user,
        ], 'Logged In');
    }

    public function register(Request $request)
    {
        $validatedData = $this->validate($request);
        User::create($validatedData);

        return $this->success([], 'Registered Successfully');
    }

    public function verifyEmail(Request $request)
    {
        $validatedData = $this->validate($request);

        $user = User::where([['email', $validatedData['email']], ['otp', $validatedData['otp']]])->first();

        if (!$user) {
            return $this->error("No OTP found");
        }

        $user->update([
            'email_verified_at' => now(),
            'otp' => null
        ]);

        return $this->success([
            'token' => $user->createToken($user->email)->plainTextToken,
            'user' => $user
        ], 'Verified Successfully');
    }

    public function logout()
    {
        auth()->user()->currentAccessToken()->delete();
        return $this->success([], 'Logged Out Successfully');
    }

    public function me()
    {
        if (auth()->check()) {
            return $this->success([], 'Valid Token');
        }

        return $this->error('No Longer Authenticated');
    }
}
