<?php

use Illuminate\Support\Facades\Route;
use Klev\UserAuthentication\UserController;

Route::group(['middleware' => 'api', 'prefix' => 'api/'],function () {
    Route::post('login', [UserController::class, 'login']);
    Route::post('register', [UserController::class, 'register']);
    Route::post('check-email', [UserController::class, 'checkEmail']);
    Route::post('verify-email', [UserController::class, 'verifyEmail']);
});
